package org.activiti.explorer.conf;

import org.springframework.context.annotation.*;

@Configuration
@PropertySources({
        @PropertySource(value = "classpath:configuration.properties", ignoreResourceNotFound = false),
        @PropertySource(value = "classpath:db.properties", ignoreResourceNotFound = false),
        @PropertySource(value = "classpath:engine.properties", ignoreResourceNotFound = false)
})
@ComponentScan(basePackages = {"org.activiti.explorer.conf"})
@ImportResource({"classpath:activiti-ui-context.xml", "classpath:activiti-login-context.xml", "classpath:activiti-custom-context.xml"})
public class ApplicationConfiguration {

}
