/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.invoker;

import it.lutech.c2sense.pee.Constant;
import org.reficio.ws.SoapContext;
import org.reficio.ws.builder.SoapBuilder;
import org.reficio.ws.builder.SoapOperation;
import org.reficio.ws.builder.core.Wsdl;
import org.reficio.ws.client.core.SoapClient;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by massimo on 13/05/16.
 */
public class SOAPInvokerImpl implements Invoker, Constant {

    private static Logger logger = Logger.getLogger(SOAPInvokerImpl.class.getName());

    public String run(String businessName, String serviceKey, String bindingKey, String operationName, String[] params) {
        return run(businessName, serviceKey, bindingKey, operationName, params, false);
    }

    @Override
    public String run(String businessName, String serviceKey, String bindingKey, String operationName, String[] params, boolean test) {

        try {
            logger.info("#####################################################################");
            logger.info("### businessName  : " + businessName);
            logger.info("### serviceKey    : " + serviceKey);
            logger.info("### bindingKey    : " + bindingKey);
            logger.info("### operationName : " + operationName);
            logger.info("#####################################################################");
            Map<String, String> toReturn = lookupService(businessName, serviceKey, bindingKey);

            String varWsdlUri = toReturn.get(VAR_WSDL_URI);

            Wsdl wsdl = Wsdl.parse(varWsdlUri);

            List<QName> bindings = wsdl.getBindings();
            if(bindings.size() != 1){
                throw new Exception("jUDDI binding configuration error: service bindings must be one, found: " + bindings.size());
            }
            String localPart = bindings.get(0).getLocalPart();
            SoapBuilder builder = wsdl.binding()
                    .localPart(localPart)
                    .find();

            String response;
            if(!test) {
                SoapOperation operation = builder.operation()
                        .name(operationName)
                        .find();

                String request = builder.buildInputMessage(operation, SoapContext.NO_CONTENT);
                logger.info("########************************* ");
                logger.info("######## request:\n" + request);
                logger.info("########************************* ");
                request = insertVariables(params, request);

                List<String> serviceUrls = builder.getServiceUrls();
                if (serviceUrls.size() != 1) {
                    throw new Exception("jUDDI builder configuration error: service urls must be one, found: " + serviceUrls.size());
                }
                String endpointUri = serviceUrls.get(0);
                SoapClient.Builder clientBuilder = SoapClient.builder()
                        .endpointUri(endpointUri);

                SoapClient client = clientBuilder.build();
                response = client.post(request);

                logger.info(response);
                client.disconnect();
            } else {
                response = "";
            }

            return response;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    private String insertVariables(String[] params, String request) {
        if(params != null) {
            for (String param : params) {
                if (request.contains("?")) {
                    request = request.replaceFirst("\\?", param);
                } else {
                    logger.warning("Create request error. Parameters exceeded");
                }
            }
            logger.info("########************************* ");
            logger.info("######## request with variables:\n" + request);
            logger.info("########************************* ");
        }
        if(request.contains("?")){
            logger.warning("Request string uncomplete. Insufficient parameters");
        }
        return request;
    }

    private Map<String, String> lookupService(final String businessName, final String serviceKey, final String bindingKey) throws Exception {
        Map<String, String> toReturn = new HashMap<>();

        UddiLookupService instance = new UddiLookupService();
        Map<String, String> map = instance.lookup(businessName, serviceKey, bindingKey);
        logger.info("################################################");
        logger.info("### service information:");
        for (String key: map.keySet()){
            logger.info("### key  : " + key);
            logger.info("### value: " + map.get(key));
        }
        logger.info("################################################");
        toReturn.putAll(map);

        return toReturn;
    }
}
