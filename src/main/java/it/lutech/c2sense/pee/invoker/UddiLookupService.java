/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.invoker;

import it.lutech.c2sense.pee.Constant;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.juddi.api_v3.AccessPointType;
import org.apache.juddi.v3.client.UDDIConstants;
import org.apache.juddi.v3.client.config.UDDIClient;
import org.apache.juddi.v3.client.transport.Transport;
import org.apache.juddi.v3.client.transport.TransportException;
import org.uddi.api_v3.*;
import org.uddi.v3_service.UDDIInquiryPortType;
import org.uddi.v3_service.UDDISecurityPortType;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by massimo on 13/05/16.
 */
public class UddiLookupService {

    private static Logger logger = Logger.getLogger(UddiLookupService.class.getName());

    private static UDDISecurityPortType security;
    private static UDDIInquiryPortType inquiry;

    private String userId;
    private String credential;

    public UddiLookupService() throws TransportException, ConfigurationException {

        logger.info("*************************************");
        logger.info("*** Setting up the service...");

        logger.info("*** Use uddi.xml");
        // create a client and read the config in the archive;
        // you can use your config file name
        UDDIClient uddiClient = new UDDIClient("uddi.xml");

        // a UddiClient can be a client to multiple UDDI nodes, so
        // supply the nodeName (defined in your uddi.xml.
        // The transport can be WS, inVM, RMI etc which is defined in the uddi.xml
        String nodeName = uddiClient.getClientConfig().getUDDINodeList().get(0).getName();
        logger.info("*** Node: " + nodeName);

        Properties properties = uddiClient.getClientConfig().getUDDINode(nodeName).getProperties();
        userId = properties.getProperty("basicAuthUsername");
        credential = properties.getProperty("basicAuthPassword");

        Transport transport = uddiClient.getTransport(nodeName);

        // Now you create a reference to the UDDI API
        security = transport.getUDDISecurityService();
        inquiry = transport.getUDDIInquiryService();

        logger.info("*** Setting up the service... done!");
        logger.info("*************************************");
    }

    public Map<String, String> lookup(String businessName, String serviceKey, String bindingKey) throws Exception {
        logger.info("Lookup service");
        logger.info("     businessName: " + businessName);
        logger.info("     serviceKey: " + serviceKey);
        logger.info("     bindingKey : " + bindingKey);
        String token = getAuthToken();
        Map<String, String> toReturn = findService(token, businessName, serviceKey, bindingKey);
        discardAuthToken(token);
        return toReturn;
    }

    public String getAuthToken() throws RemoteException {
        GetAuthToken getAuthToken = new GetAuthToken();
        getAuthToken.setUserID(userId);
        getAuthToken.setCred(credential);
        AuthToken authToken = security.getAuthToken(getAuthToken);
        logger.info("Login successful!");

        return authToken.getAuthInfo();
    }

    public void discardAuthToken(String token) throws RemoteException {
        security.discardAuthToken(new DiscardAuthToken(token));
        logger.info("Logged out");
    }

    public Map<String, String> findService(String token, String businessName, String serviceKey, String bindingKey) throws Exception {
        BusinessList businessList = getBusinessList(token, businessName, 0, 100);
        if(businessList.getBusinessInfos() == null){
            throw new Exception("No info found with businessName: " + businessName);
        }
        return fetchServiceDetailsByBusiness(businessList.getBusinessInfos(), serviceKey, bindingKey, token);
    }

    /**
     * Find all of the registered businesses. This list may be filtered
     * based on access control rules
     *
     * @param token
     * @return
     * @throws Exception
     */
    private BusinessList getBusinessList(String token, String query, int offset, int maxrecords) throws Exception {
        FindBusiness findBusiness = new FindBusiness();
        findBusiness.setAuthInfo(token);
        FindQualifiers findQualifiers = new org.uddi.api_v3.FindQualifiers();
        findQualifiers.getFindQualifier().add(UDDIConstants.APPROXIMATE_MATCH);

        findBusiness.setFindQualifiers(findQualifiers);
        Name searchName = new Name();
        if (query == null || query.equalsIgnoreCase("")) {
            searchName.setValue(UDDIConstants.WILDCARD);
        } else {
            searchName.setValue(query);
        }
        findBusiness.getName().add(searchName);
        findBusiness.setListHead(offset);
        findBusiness.setMaxRows(maxrecords);
        BusinessList businessList = inquiry.findBusiness(findBusiness);
        return businessList;

    }

    /**
     * Converts category bags of tmodels to a readable string
     *
     * @param categoryBag
     * @return
     */
    private String catBagToString(CategoryBag categoryBag) {
        StringBuilder sb = new StringBuilder();
        if (categoryBag == null) {
            return "no data";
        }
        for (int i = 0; i < categoryBag.getKeyedReference().size(); i++) {
            sb.append(keyedReferenceToString(categoryBag.getKeyedReference().get(i)));
        }
        for (int i = 0; i < categoryBag.getKeyedReferenceGroup().size(); i++) {
            sb.append("Key Ref Grp: TModelKey=");
            for (int k = 0; k < categoryBag.getKeyedReferenceGroup().get(i).getKeyedReference().size(); k++) {
                sb.append(keyedReferenceToString(categoryBag.getKeyedReferenceGroup().get(i).getKeyedReference().get(k)));
            }
        }
        return sb.toString();
    }

    private String keyedReferenceToString(KeyedReference item) {
        StringBuilder sb = new StringBuilder();
        sb.append("Key Ref: Name=").
                append(item.getKeyName()).
                append(" Value=").
                append(item.getKeyValue()).
                append(" tModel=").
                append(item.getTModelKey()).
                append(System.getProperty("line.separator"));
        return sb.toString();
    }

    private void fillServiceDetail(Map<String, String> toReturn, BusinessService get, String bindingKey) {
        if (get == null) {
            return;
        }

        String name = listToString(get.getName());
        logger.info("=== Name   : " + name);
        toReturn.put("Name", name);

        String description = listToDescString(get.getDescription());
        logger.info("=== Desc   : " + description);
        toReturn.put("Desc", description);

        logger.info("=== Key    : " + get.getServiceKey());
        toReturn.put("Key", get.getServiceKey());

        String categoryBag = catBagToString(get.getCategoryBag());
        logger.info("=== Cat bag: " + categoryBag);
        toReturn.put("CatBag", categoryBag);

        if (!get.getSignature().isEmpty()) {
            logger.info("=== Item is digitally signed");
            toReturn.put("Signed", "true");
        } else {
            logger.info("=== Item is not digitally signed");
            toReturn.put("Signed", "false");
        }

        fillBindingTemplates(toReturn, get.getBindingTemplates(), bindingKey);
    }

    /**
     * This function is useful for translating UDDI's somewhat complex data
     * format to something that is more useful.
     *
     * @param toReturn
     * @param bindingTemplates
     */
    private void fillBindingTemplates(Map<String, String> toReturn, BindingTemplates bindingTemplates, String bindingKey) {
        if (bindingTemplates == null) {
            return;
        }
        for (int i = 0; i < bindingTemplates.getBindingTemplate().size(); i++) {
            logger.info("====== Binding Key: " + bindingTemplates.getBindingTemplate().get(i).getBindingKey());
            if (bindingTemplates.getBindingTemplate().get(i).getBindingKey().equals(bindingKey)) {
                if (bindingTemplates.getBindingTemplate().get(i).getAccessPoint() != null) {
                    logger.info("====== Access Point: " + bindingTemplates.getBindingTemplate().get(i).getAccessPoint().getValue() + " type " + bindingTemplates.getBindingTemplate().get(i).getAccessPoint().getUseType());
                    if (bindingTemplates.getBindingTemplate().get(i).getAccessPoint().getUseType() != null) {
                        if (bindingTemplates.getBindingTemplate().get(i).getAccessPoint().getUseType().equalsIgnoreCase(AccessPointType.WSDL_DEPLOYMENT.toString())) {
                            //Use this access point value as a URL to a WSDL document, which presumably will have a real access point defined
                            toReturn.put(Constant.VAR_WSDL_URI, bindingTemplates.getBindingTemplate().get(i).getAccessPoint().getValue());
                        }
                    }
                }
            }
        }
    }

    private String listToString(List<Name> name) {
        StringBuilder sb = new StringBuilder();
        for (Name aName : name) {
            sb.append(aName.getValue()).append(" ");
        }
        return sb.toString();
    }

    private String listToDescString(List<Description> name) {
        StringBuilder sb = new StringBuilder();
        for (Description aName : name) {
            sb.append(aName.getValue()).append(" ");
        }
        return sb.toString();
    }

    private Map<String, String> fetchServiceDetailsByBusiness(BusinessInfos businessInfos, String serviceKey, String bindingKey, String token) throws Exception {
        Map<String, String> toReturn = new HashMap<>();
        for (int i = 0; i < businessInfos.getBusinessInfo().size(); i++) {
            GetServiceDetail gsd = new GetServiceDetail();
            gsd.getServiceKey().add(serviceKey);
            gsd.setAuthInfo(token);
            logger.info("Fetching data for business " + businessInfos.getBusinessInfo().get(i).getBusinessKey());
            ServiceDetail serviceDetail = inquiry.getServiceDetail(gsd);
            for (int k = 0; k < serviceDetail.getBusinessService().size(); k++) {
                fillServiceDetail(toReturn, serviceDetail.getBusinessService().get(k), bindingKey);
            }
        }

        return toReturn;
    }

    /*public static void main(String[] args) {
        try {
            UddiLookupService instance = UddiLookupService.getInstance();
            String businessName = "c2sense.org";

            String serviceKey = "uddi:c2sense.org:c2esb-service-manager";
            String bindingKey = "uddi:c2sense.org:c2esb-service-manager-binding-ws";
            Map<String, String> map = instance.lookup(businessName, serviceKey, bindingKey);
            for (String key: map.keySet()){
                System.out.println("########################");
                System.out.println("### key  : " + key);
                System.out.println("### value: " + map.get(key));
            }

            System.out.println("########################");
            System.out.println("" + Constant.VAR_WSDL_URI + ": " + map.get(Constant.VAR_WSDL_URI));

            serviceKey = "uddi:c2sense.org:ipgw-service";
            bindingKey = "uddi:c2sense.org:ipgw-service-binding-ws";
            map = instance.lookup(businessName, serviceKey, bindingKey);
            for (String key: map.keySet()){
                System.out.println("########################");
                System.out.println("### key  : " + key);
                System.out.println("### value: " + map.get(key));
            }

            System.out.println("########################");
            System.out.println("" + Constant.VAR_WSDL_URI + ": " + map.get(Constant.VAR_WSDL_URI));
        } catch (ServiceConfigurationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
