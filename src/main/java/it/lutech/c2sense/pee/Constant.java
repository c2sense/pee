/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee;

/**
 * Created by massimo on 20/05/16.
 */
public interface Constant {
    public static final String SEPARATOR = ";";
    public static final String PIPE_SEPARATOR = "\\|";
    public static final String PARAGRAPH_SEPARATOR = "\\§";
    public static final String HASH_SEPARATOR = "#";

    public static String VAR_WSDL_URI = "VAR_WSDL_URI";

    public static String VAR_VALUES = "VAR_VALUES";

    //TRUE|FALSE
    public static String USE_PROXY = "USE_PROXY";
    public static String PROXY_HOST = "PROXY_HOST";
    public static String PROXY_PORT = "PROXY_PORT";
    public static String PROXY_AUTH_USER = "PROXY_AUTH_USER";
    public static String PROXY_AUTH_PASSWORD = "PROXY_AUTH_PASSWORD";

}
