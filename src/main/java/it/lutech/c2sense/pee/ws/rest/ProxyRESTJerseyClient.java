/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.ws.rest;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import it.lutech.c2sense.pee.Constant;
import it.lutech.c2sense.pee.model.IPGWData;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by massimo on 30/05/16.
 */
public class ProxyRESTJerseyClient {

    private static Logger logger = Logger.getLogger(ProxyRESTJerseyClient.class.getName());

    public String callRestService(String params) throws Exception {

        logger.info("##################################");
        logger.info("######## callRestService: " + params);
        logger.info("##################################");

        Client client = Client.create();

        String method = extractParam(params, "method");

        WebResource webResource = client.resource(extractParam(params, "uri"));

        ClientResponse response;
        if(method.equalsIgnoreCase("GET")) {
            response = webResource.accept("application/json").get(ClientResponse.class);
        } else if(method.equalsIgnoreCase("PUT")) {
            String input = extractParam(params, "body");

            response = webResource.type("text/plain")
                    .put(ClientResponse.class, input);
        } else /*if(method.equalsIgnoreCase("POST"))*/ {
            String input = extractParam(params, "body");

            response = webResource.type("application/json")
                    .post(ClientResponse.class, input);
        }

        if(response.getStatus() != 200){
            throw new RuntimeException("" + response.getStatus());
        }

        String result = response.getEntity(String.class);
        logger.info("######## Response Status: " + response.getStatus());
        logger.info("######## Response: " + result);

        if (response.getStatus() != 200) {
            logger.severe("Server error! \n");
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        return convertToJSON(result);

    }

    private String convertToJSON(String result) {
        int start = result.indexOf("body") + 5;
        if(start >= 5) {
            int end = result.indexOf("body", start) - 2;
            String requestBody = result.substring(start, end);
            logger.info(requestBody);
            requestBody = requestBody.replaceAll("<br />", "<br/>");
            String[] split = requestBody.split("<br/>");

            List<IPGWData> list = new ArrayList<>();
            IPGWData data;
            for (int i = 0; i < split.length; i++) {
                data = new IPGWData();
                start = 0;
                end = split[i].indexOf("period");
                data.setId(split[i].substring(start, end).trim());
                start = end + 7;
                end = split[i].indexOf("mute");
                data.setPeriod(Integer.parseInt(split[i].substring(start, end).trim()));
                start = end + 5;
                end = split[i].length();
                data.setMute(Boolean.parseBoolean(split[i].substring(start, end).trim()));
                list.add(data);
            }

            Gson converter = new Gson();
            return converter.toJson(list);
        } else {
            return result;
        }
    }

    private String extractParam(String params, String paramName) throws Exception {
        String[] split = params.split(Constant.PARAGRAPH_SEPARATOR);
        String[] names = split[0].split(Constant.HASH_SEPARATOR);
        String[] values = split[1].split(Constant.HASH_SEPARATOR);
        for (int i = 0; i < names.length; i++) {
            if(names[i].equals(paramName))
                return values[i];
        }
        throw new Exception("uri parameter not found");
    }

}