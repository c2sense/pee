/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

//Service Endpoint Interface
@WebService
@SOAPBinding(style = Style.RPC)
public interface PeeWS {

    @WebMethod
    String importProfile(byte[] fileContent, String fileName);

    @WebMethod
    String deleteProfile(String processId, Boolean cascade);

    @WebMethod
    String startProfile(String processId, String businessKey, String variables);

    @WebMethod
    String completeUserTask(String processId, String taskDefinitionKey);

    @WebMethod
    String restProxy(String params);

    @WebMethod
    String mockSendMissionPlan(String message, String params);

    @WebMethod
    String mockSendMissionPlanAcknowledgement(String dateTime);

    @WebMethod
    String mockSendSensorConfiguration(String message, String params);

    @WebMethod
    String mockSendSensorConfigurationAcknowledgement(String dateTime);

    @WebMethod
    String mockSendSensorData(String message, String params);

    @WebMethod
    String mockSendManagementReport(String report, String params);

    @WebMethod
    String mockSendAlert(String dateTime);

}