/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.ws;

import it.lutech.c2sense.pee.engine.Executor;
import it.lutech.c2sense.pee.ws.rest.ProxyRESTJerseyClient;

import javax.jws.WebService;
import java.util.logging.Level;
import java.util.logging.Logger;

//Service Implementation
@WebService(endpointInterface = "it.lutech.c2sense.pee.ws.PeeWS")
public class PeeWSImpl implements PeeWS {

    private static Logger logger = Logger.getLogger(PeeWSImpl.class.getName());
    
    @Override
    public String importProfile(byte[] fileContent, String fileName) {
        logger.info("###################################");
        logger.info("incoming call for importProfile");
        logger.info("###################################");
        Executor.importProfile(fileContent, fileName);

        return "200";
    }

    @Override
    public String deleteProfile(String processId, Boolean cascade) {
        logger.info("###################################");
        logger.info("incoming call for deleteProfile");
        logger.info("###################################");
        Executor.deleteProfile(processId, cascade);

        return "200";
    }

    @Override
    public String startProfile(String processId, String businessKey, String variables) {
        logger.info("###################################");
        logger.info("incoming call for startProfile");
        logger.info("###################################");
        Executor.startProfile(processId, businessKey, variables);

        return "200";
    }

    @Override
    public String completeUserTask(String processId, String taskDefinitionKey) {
        logger.info("###################################");
        logger.info("incoming call for completeUserTask");
        logger.info("###################################");
        Executor.completeUserTask(processId, taskDefinitionKey);

        return "200";
    }

    @Override
    public String restProxy(String params) {
        logger.info("###################################");
        logger.info("incoming call for restProxy");
        logger.info("###################################");
        ProxyRESTJerseyClient client = new ProxyRESTJerseyClient();

        try {
            return client.callRestService(params);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            return e.getMessage();
        }

    }

    @Override
    public String mockSendMissionPlan(String message, String params) {
        logger.info("###################################");
        logger.info("incoming call for mockSendMissionPlan");
        logger.info("###################################");
        Executor.mockSendMissionPlan(message, params);

        return "200";
    }

    @Override
    public String mockSendMissionPlanAcknowledgement(String dateTime) {
        logger.info("###################################");
        logger.info("incoming call for mockSendMissionPlanAcknowledgement");
        logger.info("###################################");
        Executor.mockSendMissionPlanAcknowledgement(dateTime);

        return "200";
    }

    @Override
    public String mockSendSensorConfiguration(String message, String params) {
        logger.info("###################################");
        logger.info("incoming call for mockSendSensorConfiguration");
        logger.info("###################################");
        Executor.mockSendSensorConfiguration(message, params);

        return "200";
    }

    @Override
    public String mockSendSensorConfigurationAcknowledgement(String dateTime) {
        logger.info("###################################");
        logger.info("incoming call for mockSendSensorConfigurationAcknowledgement");
        logger.info("###################################");
        Executor.mockSendSensorConfigurationAcknowledgement(dateTime);

        return "200";
    }

    @Override
    public String mockSendSensorData(String message, String params) {
        logger.info("###################################");
        logger.info("incoming call for mockSendSensorData");
        logger.info("###################################");
        Executor.mockSendSensorData(message, params);

        return "200";
    }

    @Override
    public String mockSendManagementReport(String report, String params) {
        logger.info("###################################");
        logger.info("incoming call for mockSendManagementReport");
        logger.info("###################################");
        Executor.mockSendManagementReport(report, params);

        return "200";
    }

    @Override
    public String mockSendAlert(String dateTime) {
        logger.info("###################################");
        logger.info("incoming call for mockSendAlert");
        logger.info("###################################");
        Executor.mockSendAlert(dateTime);

        return "200";
    }
}