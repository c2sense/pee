/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by massimo on 07/06/16.
 */
@XmlRootElement(name="IPGW-info")
@XmlAccessorType(XmlAccessType.FIELD)
public class IPGWInfo {

    @XmlElement
    private Sensors sensors;

    public Sensors getSensors() {
        return this.sensors;
    }

    public void setSensors(Sensors sensors) {
        this.sensors = sensors;
    }

    @Override
    public String toString() {
        return "IPGWInfo{" +
                "sensors=" + sensors +
                '}';
    }

    /*public static void main(String[] args) {
        IPGWInfo ipgwInfo = new IPGWInfo();
        Sensors sensors = new Sensors();
        ipgwInfo.setSensors(sensors);

        Sensor sensor = new Sensor();
        sensors.getSensors().add(sensor);

        sensor.setId(102);
        sensor.setPeriod(60);
        sensor.setMute(false);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(IPGWInfo.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            StringWriter sw = new StringWriter();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            jaxbMarshaller.marshal(ipgwInfo, sw);

            System.out.println(sw.toString());

            InputStream is = new ByteArrayInputStream(sw.toString().getBytes());
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            IPGWInfo ipgwInfo2 = (IPGWInfo) jaxbUnmarshaller.unmarshal(is);
            System.out.println(ipgwInfo2);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }*/
}
