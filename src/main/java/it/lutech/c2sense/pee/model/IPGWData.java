/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.model;

/**
 * Created by massimo on 31/05/16.
 */
public class IPGWData {
    private String id;
    private Integer period;
    private boolean mute;

    public IPGWData() {}

    public IPGWData(String id, Integer period, boolean mute) {
        this.id = id;
        this.period = period;
        this.mute = mute;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPeriod() {
        return this.period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public boolean isMute() {
        return this.mute;
    }

    public void setMute(boolean mute) {
        this.mute = mute;
    }
}
