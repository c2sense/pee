/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.pee.engine;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.zip.ZipInputStream;

/**
 * Created by massimo on 12/05/16.
 */
public class Executor {

    private static Logger logger = Logger.getLogger(Executor.class.getName());

    public static void importProfile(byte[] fileContent, String fileName) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();

        if (isEmpty(fileName) || !(
                fileName.endsWith(".bpmn20.xml") ||
                        fileName.endsWith(".bpmn") ||
                        fileName.toLowerCase().endsWith(".zip"))) {

            throw new IllegalArgumentException("fileName not allowed [" + fileName + "]");
        }

        if (fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn")) {
            deploymentBuilder.addInputStream(fileName, new ByteArrayInputStream(fileContent));
        } else if (fileName.toLowerCase().endsWith(".zip")) {
            deploymentBuilder.addZipInputStream(new ZipInputStream(new ByteArrayInputStream(fileContent)));
        } else {
            throw new IllegalArgumentException("File must be of type .bpmn20.xml, .bpmn or .zip");
        }
        deploymentBuilder.name(fileName);

        Deployment deployment = deploymentBuilder.deploy();

        logger.info(" ============== DeploymentId: " + deployment.getId());
        logger.info(" ============== Name: " + deployment.getName());
        logger.info(" ============== Category: " + deployment.getCategory());
    }

    public static void deleteProfile(String processId, Boolean cascade) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();

        logger.info(" ============== PEE is deleting a process of [" + processId + "] ...");
        Deployment deployment = repositoryService.createDeploymentQuery().processDefinitionKey(processId).singleResult();
        repositoryService.deleteDeployment(deployment.getId(), cascade);
        logger.info(" ============== PEE is deleting a process of [" + processId + "] ...done!");

    }

    public static void startProfile(String processId, String businessKey, String variables) {
        Map<String, Object> map = new HashMap<>();
        if (!"".equals(variables)) {
            String[] split = variables.split(";");
            logger.info(" ============== Found " + split.length + " variables:");
            for (int i = 0; i < split.length; i++) {
                logger.info(" ============== var_" + split[i]);
                map.put("var_" + i, split[i]);
            }
        }
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();

        logger.info(" ============== PEE is creating a new instance process of [" + processId + "] ...");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processId, String.valueOf(businessKey), map);
        logger.info(" ============== PEE is creating a new instance process of [" + processId + "] ...done!");
        logger.info(" ============== processInstance.getProcessDefinitionId [" + processInstance.getProcessDefinitionId() + "]");
        logger.info(" ============== processInstance.getBusinessKey [" + processInstance.getBusinessKey() + "]");

    }

    public static void completeUserTask(String processId, String taskDefinitionKey) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();

        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processDefinitionKey(processId).singleResult();
        TaskService taskService = processEngine.getTaskService();
        Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).taskDefinitionKey(taskDefinitionKey).singleResult();
        taskService.complete(task.getId());

    }

    /**
     * Mock service for demo pourpose
     * @param params
     */
    public static void mockSendMissionPlan(String message, String params) {
        logMessageAndParams(message, params);
    }

    public static void mockSendMissionPlanAcknowledgement(String dateTime) {
        logger.info(" ============== dateTime: " + dateTime);
    }

    public static void mockSendSensorConfiguration(String message, String params) {
        logMessageAndParams(message, params);
    }

    public static void mockSendSensorConfigurationAcknowledgement(String dateTime) {
        logger.info(" ============== dateTime: " + dateTime);
    }

    public static void mockSendSensorData(String message, String params) {
        logMessageAndParams(message, params);
    }

    public static void mockSendManagementReport(String report, String params) {
        logMessageAndParams(report, params);
    }

    public static void mockSendAlert(String dateTime) {
        logger.info(" ============== dateTime: " + dateTime);
    }

    private static boolean isEmpty(String fileName) {
        if (fileName == null || "".equals(fileName))
            return true;
        return false;
    }

    private static void logMessageAndParams(String message, String params) {
        logger.info(" ============== message: " + message);
        if (!"".equals(params)) {
            String[] split = params.split(";");
            logger.info(" ============== Found " + split.length + " params:");
            for (int i = 0; i < split.length; i++) {
                logger.info(" ============== " + split[i]);
            }
        }
    }
}
