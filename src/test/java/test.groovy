import it.lutech.c2sense.pee.invoker.Invoker
import it.lutech.c2sense.pee.invoker.SOAPInvokerImpl
import org.junit.Test
/**
 * Created by massimo on 31/05/16.
 */
class test {
    @Test
    public void testScript() throws Exception {
        def businessName = "c2sense.org"
        def serviceKey = "uddi:c2sense.org:ipgw-service"
        def bindingKey = "uddi:c2sense.org:ipgw-service-binding-ws"
        def operatioName = "infoxml"
        def params = null

        Invoker invoker = new SOAPInvokerImpl()

        def response = invoker.run(businessName, serviceKey, bindingKey, operatioName, params)
        println ('response: ' + response)

        def rootNode = new XmlSlurper().parseText(response)

        rootNode.sensors.each {
            operatioName = "byid"
            if(it.mute) {
                /* ************************************************
                    2. put on single sensor with short period (60)
                *************************************************** */
                params = new String[2]
                params[0] = '' + it.id
                params[1] = '{"period":"P60S","mute":"False"}'
                response = invoker.run(businessName, serviceKey, bindingKey, operatioName, params)
                println(response)
            }
        }
        //println(rootNode.soapenv:Envelope.soapenv:Header.soapenv:Body.web:GetUKLocationByPostCode.webSmiley TongueostCode.text())

        /*XmlParser parser = new XmlParser()
        def root = parser.parseText (response)
        root.value()
        println("ssss")*/
    }
}
