package it.lutech.c2sense.pee.activiti.test;

import org.activiti.engine.impl.persistence.entity.TableDataManager;
import org.activiti.engine.impl.test.PluggableActivitiTestCase;
import org.activiti.engine.test.Deployment;
import org.activiti.engine.test.api.runtime.DummySerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * User:      Massimo Sorbara
 * Date:      6/03/14
 * Project:   c2sense
 * Copyright: Lutech S.p.A. 2016
 */
public class RuntimeServiceTest extends PluggableActivitiTestCase {

	@Deployment(resources={"it/lutech/c2sense/pee/activiti/test/Simple_Alert_Scenario.bpmn20.xml"})
	public void testStartProcessInstanceWithVariables() {
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("basicType", new DummySerializable());
		runtimeService.startProcessInstanceByKey("process_Fire", vars);

		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/Simple_Alert_Scenario_2.bpmn20.xml"})
	public void testSimpleAlertScenario2() {
		runtimeService.startProcessInstanceByKey("process_Fire_Ever");

		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/DemoProfile.bpmn20.xml"})
	public void testDemoProfileLocal() {
		runtimeService.startProcessInstanceByKey("process");
        TableDataManager b;
		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/Demo_Profile_Complete.bpmn20.xml"})
	public void testDemoProfileComplete() {
		runtimeService.startProcessInstanceByKey("process");

		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/DemoProfileWithService.bpmn20.xml"})
	public void testDemoProfileWithService() {
		runtimeService.startProcessInstanceByKey("process");

		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/Sample_Flood_Alert_Scenario_Last.bpmn20.xml"})
	public void testSampleFloodAlertScenarioLast() {
		runtimeService.startProcessInstanceByKey("Process_National_Weather_Service");

		assertEquals(1, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/SampleFloodAlertScenario.bpmn20.xml"})
	public void testSampleFloodAlertScenario() {
		runtimeService.startProcessInstanceByKey("process_start");

		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/process.bpmn20.xml"})
	public void testProcess() {
		runtimeService.startProcessInstanceByKey("process_start");

		assertEquals(1, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/PilotScenario.bpmn20.xml"})
	public void testPilotScenario() {
		runtimeService.startProcessInstanceByKey("process_PilotScenario");

		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

	@Deployment(resources = {"it/lutech/c2sense/pee/activiti/test/Micro-scenario2-3.bpmn20.xml"})
	public void testMicroScenario2_3() {
		runtimeService.startProcessInstanceByKey("Process_Micro-scenario2-3");

		assertEquals(0, runtimeService.createProcessInstanceQuery().count());
	}

}
