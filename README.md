# PEE

## Overall description

Profile Execution Engine executes the profiles specialized for specific organizations and incidents.

PEE is a Web Service that wraps the underlying Activiti, the business process engine of business processes Alfresco.

## Installation Guide ##

Read the [Installation Guide][1].

## Developer Guide ##

Read the [Developer Guide][2].

## Explorer ##

Read the [Explorer][3] documentation.

##License##

Licensed under the [Apache License, Version 2.0][4]

[1]: docs/installation-guide.md
[2]: docs/developer-guide.md
[3]: docs/explorer.md
[4]: http://www.apache.org/licenses/LICENSE-2.0