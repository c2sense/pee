# PEE Developer Guide

## Architecture ##

![Architecture](images/PEEArchitecture.png)

PEE wrap an Activiti Engine and exposes function for the profile execution and management.
Activiti is embedded in the application and persists his data in a schema on MySQL database.
PEE extends Activiti Explorer web application with a web services interface. Web services are implemented in JAX-WS using SOAP protocol.
PEE is composed of three main parts:

 * Execution Engine
 * Invoker
 * Activiti

## Execution Engine

The class diagram is shown in the next figure:

![Class diagram](images/ExecutionEngineClassDiagram.png)

The class PeeWS is the entry-point of the PEE and implements the SOA interface.

There are 4 operations:

> * importProfile(byte[] fileContent, String filename)
> Import a profile (fileContent) in the PEE with a specified fileName (is useful for versioning the in activiti)

> * deleteProfile(String processId, Boolean cascade)
> Remove a profile by processId, and all correlated process if cascade = true

> * startProfile(String processId, String businessKey, String variables)
> Starts a workflow identied by processId with a businessKey and variables

> * completeUserTask(String processId, String taskDefinitionKey)
> Complete a task if the process is waiting in a active task with taskDefinitionKey

PeeWS interacts with the Executor class for performing the respectively functions on the Activiti Engine.

### PeeWS: JAX-WS implementation

The Web Services Description Language (WSDL) is the XML document that describe the functions and parameters of PeeWS.
It describes also how to interact with the service and is used for the creation of the client.
Within the WSDL files are described:

 * the operations made available by the service;
 * the communication protocol to use to access the service;
 * the format of messages accepted input;
 * the returned output and their format;
 * endpoints of each function.

#### PeeWS.wsdl

    <?xml version='1.0' encoding='UTF-8'?>
    <definitions
            xmlns:wsam="http://www.w3.org/2007/05/addressing/metadata" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
            xmlns:tns="http://ws.pee.c2sense.lutech.it/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="http://ws.pee.c2sense.lutech.it/"
            name="PeeWSImplService">
        <types/>
        <message name="importProfile">
            <part name="arg0" type="xsd:base64Binary"/>
            <part name="arg1" type="xsd:string"/>
        </message>
        <message name="importProfileResponse">
            <part name="return" type="xsd:string"/>
        </message>
        <message name="deleteProfile">
            <part name="arg0" type="xsd:string"/>
            <part name="arg1" type="xsd:boolean"/>
        </message>
        <message name="deleteProfileResponse">
            <part name="return" type="xsd:string"/>
        </message>
        <message name="startProfile">
            <part name="arg0" type="xsd:string"/>
            <part name="arg1" type="xsd:string"/>
            <part name="arg2" type="xsd:string"/>
        </message>
        <message name="startProfileResponse">
            <part name="return" type="xsd:string"/>
        </message>
        <message name="completeUserTask">
            <part name="arg0" type="xsd:string"/>
            <part name="arg1" type="xsd:string"/>
        </message>
        <message name="completeUserTaskResponse">
            <part name="return" type="xsd:string"/>
        </message>
        <portType name="PeeWS">
            <operation name="importProfile" parameterOrder="arg0 arg1">
                <input wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/importProfileRequest"
                       message="tns:importProfile"/>
                <output wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/importProfileResponse"
                        message="tns:importProfileResponse"/>
            </operation>
            <operation name="deleteProfile" parameterOrder="arg0 arg1">
                <input wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/deleteProfileRequest"
                       message="tns:deleteProfile"/>
                <output wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/deleteProfileResponse"
                        message="tns:deleteProfileResponse"/>
            </operation>
            <operation name="startProfile" parameterOrder="arg0 arg1 arg2">
                <input wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/startProfileRequest" message="tns:startProfile"/>
                <output wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/startProfileResponse"
                        message="tns:startProfileResponse"/>
            </operation>
            <operation name="completeUserTask" parameterOrder="arg0 arg1">
                <input wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/completeUserTaskRequest"
                       message="tns:completeUserTask"/>
                <output wsam:Action="http://ws.pee.c2sense.lutech.it/PeeWS/completeUserTaskResponse"
                        message="tns:completeUserTaskResponse"/>
            </operation>
        </portType>
        <binding name="PeeWSImplPortBinding" type="tns:PeeWS">
            <soap:binding transport="http://schemas.xmlsoap.org/soap/http" style="rpc"/>
            <operation name="importProfile">
                <soap:operation soapAction=""/>
                <input>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </input>
                <output>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </output>
            </operation>
            <operation name="deleteProfile">
                <soap:operation soapAction=""/>
                <input>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </input>
                <output>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </output>
            </operation>
            <operation name="startProfile">
                <soap:operation soapAction=""/>
                <input>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </input>
                <output>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </output>
            </operation>
            <operation name="completeUserTask">
                <soap:operation soapAction=""/>
                <input>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </input>
                <output>
                    <soap:body use="literal" namespace="http://ws.pee.c2sense.lutech.it/"/>
                </output>
            </operation>
        </binding>
        <service name="PeeWSImplService">
            <port name="PeeWSImplPort" binding="tns:PeeWSImplPortBinding">
                <soap:address location="http://localhost:8080/pee/ws/PeeWS"/>
            </port>
        </service>
    </definitions>

### Executor

The class Executor handles interaction with Activiti Engine.
It receives parameters from PeeWS and performs the work of Activiti by implementing the following methods:
> * public static void importProfile(byte[] fileContent, String fileName);

> * public static void deleteProfile(String processId, Boolean cascade);

> * public static void startProfile(String processId, String businessKey, String[] variables);

> * public static void completeUserTask(String processId, String taskDefinitionKey);

## Invoker

The class diagram is shown in the next figure:

![Class diagram](images/InvokerClassDiagram.png)

SOAPInvokerImpl implements the org.activiti.engine.delegate.JavaDelegate interface. A SeviceTask can use it for listener class to invoke a SOA service.

Workflow developer can use the following variable for passaing parameter to invoker:

    //separator character for VARS_NAME, VARS_VALUE and OUT_VAR_NAME
    public static final String SEPARATOR = ";";

    //juddi service name
    public static String VAR_SERVICE_NAME = "VAR_SERVICE_NAME";

    //ex: http://www.webservicex.net/CurrencyConvertor.asmx
    public static String VAR_URI = "VAR_URI";

    //ex: http://www.webservicex.net/CurrencyConvertor.asmx?WSDL
    public static String VAR_WSDL_URI = "VAR_WSDL_URI";

    //ex:CurrencyConvertorSoap
    public static String VAR_BINDING = "VAR_BINDING";

    //ex:http://www.webserviceX.NET/ConversionRate
    public static String VAR_OPERATION = "VAR_OPERATION";

    public static String VARS_NAME = "VARS_NAME";
    public static String VARS_VALUE = "VARS_VALUE";
    public static String OUT_VAR_NAME = "OUT_VAR_NAME";

SOAPInvokerImpl makes use of UddiLookupService class to retrieve the object distributed on which to make the call.
If is necessary to invoke a call to a non-SOAP service, it must write a new class that implements the org.activiti.engine.delegate.JavaDelegate interface.

## Activiti

The engine API is the way of interacting with Activiti.
The central starting point is the ProcessEngine and from it, you can obtain the various services that contain the workflow/BPM methods.
With this services PEE store and delete a profile, starts a process and complete a user task.

![Activiti API Services](images/ActivitiAPIServices.png)

Time to check the Developer Guide!

##License##

Licensed under the [Apache License, Version 2.0][1]

[1]: http://www.apache.org/licenses/LICENSE-2.0