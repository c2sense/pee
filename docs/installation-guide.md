# PEE Installation Guide


## Requirements ##

The requirements to install a working copy of the PEE are:

* Oracle JDK >=1.7
* Database to install the database schema for the service: Mysql>=5.0
* Maven >= 3.0

## Installation ##

All commands shown here are ready to be executed from the root directory of the project (i.e., the one with this _README.md_ file) 

###1. Download the project ###

Clone the project using git from the [pee repository](https://Lutech-C2SENSE@bitbucket.org/Lutech-C2SENSE/pee.git)

	$ git clone https://bitbucket.org/Lutech-C2SENSE/pee.git

###2. Creating the mysql database ###

From mysql command tool, create a database (with a user with sufficient privileges, as root):

	$ mysql -p -u root 
	
	mysql> CREATE DATABASE pee;

Create a user:

	mysql> CREATE USER pee@localhost IDENTIFIED BY '_pee_';
	mysql> GRANT ALL PRIVILEGES ON pee.* TO pee@localhost; -- * optional WITH GRANT OPTION;

The pee webapp will create all the needed tables when loaded by first time.

The names used here are the default values of the pee. See [configuration](#configuration) to know how to change the values.

### 3. Importing the code into eclipse ###

The core of the Lutech PEE has been developed using the Eclipse Java IDE, although others Java editors could be used, here we only provide the instructions about how to import the code into Eclipse.

The first step is to tell Maven to create the necessary Eclipse project files executing this:

	$ mvn eclipse:eclipse

The previous command is going to generate the eclipse project files: 
.settings, .classpath, and .project. Again, please never upload those files to the version control, it is going to deconfigure the eclipse of other developers (it is easy to fix, just an annoying waste of time).

After it, from your eclipse you can import the project. Go to "import project from file", go to the base folder, and you should see the "PEE" project ready to be imported in your Eclipse. 

## Configuration ##

A _configuration.properties_, _db.properties_ and a _engine.properties_ are placed in the _conf/env/$environment_name$_ directory. Suitably customize these environments for the respective files.

### configuration.properties
For now, just a property:
1. `PeeWS.url`: url of PEE web service

### db.properties

Several parameters can be configured through this files.
1. `db=mysql`: database type
1. `jdbc.driver=com.mysql.jdbc.Driver`: driver class
1. `jdbc.url=jdbc:mysql://localhost:3306/pee`: complete url with schema name
1. `jdbc.username=pee`: username
1. `jdbc.password=_pee_`: password

### engine.properties

#### demo properties
1. `create.demo.users`: create users
1. `create.demo.definitions`: workflow definitions
1. `create.demo.models`: workflow models
1. `create.demo.reports`: report examples

#### engine properties
1. `engine.schema.update`: update the schema when activiti starts
1. `engine.activate.jobexecutor`: activate job executor
1. `engine.asyncexecutor.enabled`: async execution enable
1. `engine.asyncexecutor.activate`: async execution activation
1. `engine.history.level`: full activiti log

#### email properties
1. `engine.email.enabled`: email service enabled
1. `engine.email.host`: hostname
1. `engine.email.port`: port

If you need a new environment, after the creation of a suitable `conf/env/<target>/*.properties` file (for a `<target>` environment), you need to create a new maven profile in the _pom.xml_ file like _dev_ end _test_. 

## Compiling ##

For a correct packaging it's mandatory choose a profile for inject the parameters accordingly to the target environment.
For example for create a package to the test environment run the command: 

	$  mvn clean install -Penv-test
	
If you want to skip tests:
	
	$ mvn clean install -Penv-test -Dmaven.test.skip=true
	
The result of the command is a war in _target_. The war is also copied to the directory pointed by _tomcat.directory_ property specified in the _test.properties_ file.

## Running ##

If the war was successfully copied to _tomcat.directory_, then start your tomcat to run the server.

Some configuration parameters can be overriden using environment variables or jdk variables (over the env properties). The list of parameters overridable is:

* `DB_DRIVER`; default value is `com.mysql.jdbc.Driver`
* `DB_URL`; default value is `jdbc:mysql://${db.host}:${db.port}/${db.name}`
* `DB_USERNAME`; default value is `${db.username}`
* `DB_PASSWORD`; default value is `${db.password}`
* `DB_SHOWSQL`; default value is `${db.showSQL}`
* `BROOKLYN_URL`; default value is `${brooklyn.url}`

F.e., to use a different database configuration:

	$ export DB_URL=jdbc:mysql://mysql:3306/pee
	$ export DB_USERNAME=pee
	$ export DB_PASSWORD=<secret>

## Testing ##

Check that everything is working performing the following HTTP call:

	$ curl http://c2sense-ie.ns0.it/pee/

The actual address depends on the tomcat configuration.

Time to check the Developer Guide!

##License##

Licensed under the [Apache License, Version 2.0][1]

[1]: http://www.apache.org/licenses/LICENSE-2.0